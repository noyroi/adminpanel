from flask import Flask, session, request, flash, redirect, url_for, render_template, abort
import requests
from flask_login import LoginManager, login_required
from loginAuthentication import UserCheck
from forms import LoginForm
from DBHandler import Admin, db_session, verifyAdminPassword
from flask_paginate import Pagination
from flask_googlemaps import GoogleMaps, Map
import json
import datetime
import codecs

import json
app = Flask(__name__)
app.secret_key = "super secret key"
login_manager = LoginManager()
login_manager.init_app(app)
GoogleMaps(app,key="AIzaSyCbkog_tBWfrIvEAqJdmQSP4qmv5NNruVE")


#login verify
@login_manager.request_loader
def load_user(request):
    token = None
    if 'Authorization' in session:
        token = session['Authorization']
    if token is None:
        return None
    else:
        username,uid = token.split(":") # token is username:uid
        uid = int(uid)
        user_entry = UserCheck.get(username)
        if (user_entry is not None):
            user = UserCheck(user_entry)
            if (user.id == uid):
                return user
    return None


@app.route("/",methods=["GET", "POST"])
def login():
    token = None
    if 'Authorization' in session:
        token = session['Authorization']
    if token:
        username,uid = token.split(":") # token is username:uid
        uid = int(uid)
        user_entry = UserCheck.get(username)
        if (user_entry is not None):
            user = UserCheck(user_entry)
            if (user.id == uid):
                return redirect(url_for('mainpage'))

    form = LoginForm(request.form)
    if request.method == "POST" and form.validate():
        token = verifyAdminPassword(form.username.data, form.password.data)
        if (token):
            session['Authorization'] = token
            return redirect(url_for('mainpage'))
        else:
            flash('!שם משתמש או סיסמא לא נכונים')
    return render_template('login.html', form=form)

@app.route("/main", methods=["GET"])
@login_required
def mainpage():
    from DBHandler import getlastNGames, getlastNPlayers, getlastNCourts
    return render_template("main.html", lastGames=getlastNGames(3), lastPlayers=getlastNPlayers(3),
                           lastCourts=getlastNCourts(3))


@app.route("/deleteCourt", methods=["POST"])
@login_required
def deleteCourtById():
    if request.method == "POST":
        from DBHandler import deleteCourt
        uid = request.form.get("uid")
        uid = int(uid)
        if (deleteCourt(uid)):
            answer = "success"
        else:
            answer = "fail"
        return json.dumps({"answer": answer})

@app.route("/deleteUser",methods=["POST"])
@login_required
def deleteUserById():
    if request.method == "POST":
        from DBHandler import deleteUser
        uid = request.form.get("uid")
        uid = int(uid)
        if (deleteUser(uid)):
            answer = "success"
        else:
            answer = "fail"
        return json.dumps({"answer": answer})

@app.route("/deleteAdmin",methods=["POST"])
@login_required
def deleteAdminById():
    if request.method == "POST":
        from DBHandler import deleteAdmin
        uid = request.form.get("uid")
        uid = int(uid)
        if (deleteAdmin(uid)):
            answer = "success"
        else:
            answer = "fail"
        return json.dumps({"answer": answer})

@app.route('/users/', defaults={'page': 1}, methods=["GET"])
@app.route('/users/page/<int:page>')
@login_required
def editUsers(page):
    from forms import SearchByUsername, SearchUserByUid, CustomUserSearch
    usernameform = SearchByUsername(request.form)
    uidform = SearchUserByUid(request.form)
    customform = CustomUserSearch(request.form)
    queryString = str(request.query_string)
    queryString = queryString[2:-1]
    perPage = request.args.get("perpage")
    if (not perPage or not isint(perPage)):
        perPage = 25
    else:
        perPage = int(perPage)
    sortBy = request.args.get("sortby")
    reverse = request.args.get("reverse")
    if (reverse and isint(perPage)):
        reverse = int(reverse)
        if (reverse == 1):
            reverse = True
        else:
            reverse = False
    else:
        reverse = False
    if (not sortBy):
        sortBy = "added"
        reverse = True
    firstName = request.args.get("firstname")
    lastName = request.args.get("lastname")
    city = request.args.get("city")
    username = request.args.get("username")
    ageMin = request.args.get("agemin")
    ageMax = request.args.get("agemax")
    uid = request.args.get("uid")
    if (ageMin and isint(ageMin)):
        ageMin = int(ageMin)
    else:
        ageMin = None
    if (ageMax and isint(ageMax)):
        ageMax = int(ageMax)
    else:
        ageMax = None
    if (uid and isint(uid)):
        uid = int(uid)
    else:
        uid = None
    from DBHandler import getUsersForPage
    users, count = getUsersForPage(page, perPage, sortBy=sortBy, firstName=firstName,
                            lastName=lastName, ageMin=ageMin, ageMax=ageMax,city=city,
                                   uid=uid,username=username, reverse=reverse)
    if not users and page != 1:
        return editUsers(1)
    pagination = Pagination(page=page, per_page=perPage, total=count, href="/users/page/{0}?"+queryString, css_framework='bootstrap3')
    return render_template('users.html',
        pagination=pagination,
        users=users,
        usernameform=usernameform,
        uidform=uidform,
        customform=customform
    )

@app.route('/admins/', defaults={'page': 1}, methods=["GET"])
@app.route('/admins/page/<int:page>')
@login_required
def editAdmins(page):
    from DBHandler import getAdminPermissionLevel
    token = None
    permission = None
    if 'Authorization' in session:
        token = session['Authorization']
        username, uid = token.split(":")
        if isint(uid):
            permission = getAdminPermissionLevel(int(uid))
    if token is None:
        return render_template('admins.html',permission=None)
    from forms import SearchByUsername, SearchUserByUid
    usernameform = SearchByUsername(request.form)
    uidform = SearchUserByUid(request.form)
    queryString = str(request.query_string)
    queryString = queryString[2:-1]
    perPage = request.args.get("perpage")
    if (not perPage or not isint(perPage)):
        perPage = 25
    else:
        perPage = int(perPage)
    uid = request.args.get("uid")
    sortBy = request.args.get("sortby")
    reverse = request.args.get("reverse")
    if (reverse and isint(perPage)):
        reverse = int(reverse)
        if (reverse == 1):
            reverse = True
        else:
            reverse = False
    else:
        reverse = False
    if (not sortBy):
        sortBy = "added"
        reverse = True
    username = request.args.get("username")
    permissionlevel = request.args.get("permissionlevel")
    if (permissionlevel and isint(permissionlevel)):
        permissionlevel = int(permissionlevel)
    else:
        permissionlevel = None
    if (uid and isint(uid)):
        uid = int(uid)
    else:
        uid = None
    from DBHandler import getAdminsForPage
    admins, count = getAdminsForPage(page, perPage, sortBy=sortBy,permissionlevel=permissionlevel,
                                   uid=uid,username=username, reverse=reverse)
    if not admins and page != 1:
        return editAdmins(1)
    pagination = Pagination(page=page, per_page=perPage, total=count, href="/admins/page/{0}?"+queryString, css_framework='bootstrap3')
    return render_template('admins.html',
        pagination=pagination,
        admins=admins,
        usernameform=usernameform,
        uidform=uidform,
        permission=permission
    )

@app.route('/court/<int:uid>', methods=["GET", "POST"])
@login_required
def editSingleCourt(uid):
    lat = 32.076165
    lng = 34.776796
    zoom = 6
    markers = []
    from forms import AddCourtForm
    form = AddCourtForm(request.form)
    if request.method == "POST" and not (form.LA.data and form.LO.data):
        x, y = getGeoLocation(form)
        if (x and y):
            lat = x
            lng = y
            markers.append((lat,lng))
            zoom = 15
            form.LA.data = lat
            form.LO.data = lng
        else:
            flash('נא למלא כתובת תקינה')
    elif request.method == "POST":
        lat = form.LA.data
        lng = form.LO.data
        markers.append((lat, lng))
        zoom = 15
        if form.validate():
            from DBHandler import updateCourtByUid
            try:
                updateCourtByUid(uid, form)
                flash('!פרטי המגרש עודכנו בהצלחה')
            except Exception as e:
                if 'name' in e.args[0]:
                    flash('שם מגרש כבר קיים במערכת')
                else:
                    flash(e.args[0])
    map = Map(identifier="view-side", lat=lat, lng=lng, language="he", zoom=zoom, markers=markers)
    from DBHandler import getCourtByUid
    court = getCourtByUid(uid)
    if court:
        form.name.data = court.name
        form.city.data = court.city
        if (court.street):
            form.street.data = court.street
        if (court.streetNum):
            form.num.data = court.streetNum
        if (court.kind):
            form.kind.data = court.kind
        if (court.width):
            form.width.data = court.width
        if (court.length):
            form.length.data = court.length
        if (court.description):
            form.description.data = court.description
        if (court.phone):
            form.phone.data = court.phone
        if (court.LA):
            form.LA.data = court.LA
        if (court.LO):
            form.LO.data = court.LO
        return render_template('court_add.html', form=form, uid=uid, head="עריכת מגרש: "+str(uid), map=map)
    else:
        abort(404)

def getGeoLocation(form):
    street = ""
    key = "AIzaSyBJ4VcTQLnlNZH5qXFQx4nzwtldcLleQoA"
    if form.city.data:
        address = form.city.data+","+" ישראל"
        if form.street.data:
            street = form.street.data
            if form.num.data and isint(form.num.data):
                street += " "+ str(form.num.data)
            street +=", "
        address = street+address
        url = "https://maps.googleapis.com/maps/api/geocode/json?key="+key+"&address="+address
        serveranswer = requests.get(url).content
        answer = json.loads(serveranswer.decode("utf-8"))
        if (answer["status"] == "OK"):
            if (len(answer["results"])==1):
                result = answer["results"][0]["geometry"]["location"]
                lat = result["lat"]
                lng = result["lng"]
                return lat, lng
            else:
                return None, None
        else:
            return None, None
    else:
        return None, None


@app.route('/addCourt', methods=["GET", "POST"])
@login_required
def addNewCourt():
    lat = 32.076165
    lng = 34.776796
    zoom = 6
    markers = []
    from forms import AddCourtForm
    form = AddCourtForm(request.form)
    if request.method == "POST" and not (form.LA.data and form.LO.data):
        x, y = getGeoLocation(form)
        if (x and y):
            lat = x
            lng = y
            markers.append((lat,lng))
            zoom = 15
            form.LA.data = lat
            form.LO.data = lng
        else:
            flash('נא למלא כתובת תקינה')
    elif request.method == "POST":
        lat = form.LA.data
        lng = form.LO.data
        markers.append((lat, lng))
        zoom = 15
        if form.validate():
            from DBHandler import AddNewCourt
            try:
                AddNewCourt(form)
                flash('המגרש ' + " " + form.name.data + ' !נוצר בהצלחה')
            except Exception as e:
                if 'name' in e.args[0]:
                    flash('שם מגרש כבר קיים במערכת')
                else:
                    flash(e.args[0])
    map = Map(identifier="view-side", lat=lat, lng=lng, language="he", zoom=zoom, markers=markers)
    return render_template('court_add.html', form=form, map=map, head="הוספת מגרש חדש")

def isint(s):
    try:
        int(s)
        return True
    except ValueError:
        return False

@app.route('/user/<int:uid>', methods=["GET", "POST"])
@login_required
def editSingleUser(uid):
    from forms import EditUserForm
    form = EditUserForm(request.form)
    if request.method == "POST" and form.validate():
        if validateDate(form.birthdate.data):
            from DBHandler import updateUserByUid
            try:
                updateUserByUid(uid, form)
                flash('!פרטי המשתמש עודכנו בהצלחה')
            except Exception as e:
                if 'Email' in e.args[0]:
                    flash('דואר אלקטרוני כבר קיים במערכת')
                if 'Username' in e.args[0]:
                    flash('שם משתמש זה כבר קיים במערכת')
        else:
            flash( 'dd/mm/yyyy '+':תאריך אינו בפורמט הנכון')

    from DBHandler import getPlayerByUid
    player = getPlayerByUid(uid)
    if player:
        form.username.data = player.Username
        form.birthdate.data = player.Birthday
        form.email.data = player.Email
        form.firstname.data = player.FirstName
        form.lastname.data = player.LastName
        form.city.data = player.City
        return render_template('user_edit.html', form=form, uid=uid)
    else:
        abort(404)


@app.route('/signout',methods=["GET"])
@login_required
def signOut():
    session.clear()
    return redirect (url_for('login'))


@app.route('/addAdmin',methods=["GET","POST"])
@login_required
def addNewAdmin():
    from forms import AddAdminForm
    form = AddAdminForm(request.form)
    if request.method == "POST" and form.validate():
        from DBHandler import AddNewAdmin
        try:
            AddNewAdmin(form)
            flash('!נוצר בהצלחה' + " " + form.username.data + ' המשתמש')
        except Exception as e:
            if 'Email' in e.args[0]:
                flash('דואר אלקטרוני כבר קיים במערכת')
            elif 'Username' in e.args[0]:
                flash('שם משתמש זה כבר קיים במערכת')
            else:
                flash(e.args[0])
    return render_template('admin_add.html', form=form)

@app.route('/addUser',methods=["GET","POST"])
@login_required
def addNewPlayer():
    from forms import AddUserForm
    form = AddUserForm(request.form)
    if request.method == "POST" and form.validate():
        if validateDate(form.birthdate.data):
            from DBHandler import AddNewPlayer
            try:
                AddNewPlayer(form)
                flash('!נוצר בהצלחה'+" "+form.username.data+' המשתמש')
            except Exception as e:
                if 'Email' in e.args[0]:
                    flash('דואר אלקטרוני כבר קיים במערכת')
                elif 'Username' in e.args[0]:
                    flash('שם משתמש זה כבר קיים במערכת')
                else:
                    flash (e.args[0])
        else:
            flash( 'dd/mm/yyyy '+':תאריך אינו בפורמט הנכון')
    return render_template('user_add.html', form=form)

@app.route('/courts/', defaults={'page': 1}, methods=["GET"])
@app.route('/courts/page/<int:page>')
@login_required
def editCourts(page):
    from forms import SearchByCourtName, SearchCourtByUid, CustomCourtSearch
    courtnameform = SearchByCourtName(request.form)
    uidform = SearchCourtByUid(request.form)
    customform = CustomCourtSearch(request.form)
    queryString = str(request.query_string)
    queryString = queryString[2:-1]
    perPage = request.args.get("perpage")
    if (not perPage or not isint(perPage)):
        perPage = 25
    else:
        perPage = int(perPage)
    uid = request.args.get("uid")
    sortBy = request.args.get("sortby")
    reverse = request.args.get("reverse")
    kind = request.args.get("kind")
    if (kind == ""):
        kind = None
    street = request.args.get("street")
    num = request.args.get("num")
    city = request.args.get("city")

    if (num and isint(num)):
        num = int(num)
    else:
        num = None
    if (reverse and isint(perPage)):
        reverse = int(reverse)
        if (reverse == 1):
            reverse = True
        else:
            reverse = False
    else:
        reverse = False
    if (not sortBy):
        sortBy = "added"
        reverse = True
    courtname = request.args.get("courtname")
    if (uid and isint(uid)):
        uid = int(uid)
    else:
        uid = None
    from DBHandler import getCourtsForPage
    courts, count = getCourtsForPage(page, perPage, sortBy=sortBy,num=num,street=street,
                                     city=city,kind=kind,
                                   uid=uid,courtname=courtname, reverse=reverse)
    if not courts and page != 1:
        return editCourts(1)
    pagination = Pagination(page=page, per_page=perPage, total=count, href="/courts/page/{0}?"+queryString, css_framework='bootstrap3')
    return render_template('courts.html',
        pagination=pagination,
        courts=courts,
        courtnameform=courtnameform,
        uidform=uidform,
        customform=customform
    )


def validateDate(date):
    try:
        datetime.datetime.strptime(date, '%d/%m/%Y')
        return True
    except ValueError:
        return False

@app.teardown_appcontext
def shutdown_session(exception=None):
    db_session.remove()

if __name__ == '__main__':
    app.run(port=80, host='0.0.0.0')