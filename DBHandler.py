from sqlalchemy import create_engine, Column, Integer, String, Boolean, select, func
from sqlalchemy.dialects.mysql import DOUBLE
from sqlalchemy.orm import scoped_session, sessionmaker, Session
from sqlalchemy.ext.declarative import declarative_base
from datetime import datetime, date
import time

engine = create_engine('mysql://sql11182530:uKeui89TYK@sql11.freemysqlhosting.net/sql11182530?charset=utf8')
db_session = scoped_session(sessionmaker(autocommit=False,
                                         autoflush=False,
                                         bind=engine))

DBase = declarative_base()
DBase.query = db_session.query_property()

def init_db():
    DBase.metadata.create_all(bind=engine)

class Admin(DBase):
    __tablename__ = 'admins'
    ID = Column(Integer, primary_key=True)
    Username = Column(String(255), unique=True)
    UPassword = Column(String(255))
    Email = Column(String(255), unique=True)
    permissionLevel = Column(Integer)
    SignUpDate = Column(String(10))

    def __init__(self, username, password, id=None, email=None, permissionLevel = None, SignUpDate=None):
        self.ID = id
        self.Username = username
        self.UPassword = password
        self.Email = email
        self.permissionLevel = permissionLevel
        self.SignUpDate = SignUpDate

    def __repr__(self):
        return '<Player %r>' % (self.Username)

    def setAddedDate(self):
        self.addedDateObject = datetime.strptime(self.SignUpDate, "%d/%m/%Y")

class Player(DBase):
    __tablename__ = 'players'
    ID = Column(Integer, primary_key=True)
    Username = Column(String(255), unique=True)
    UPassword = Column(String(255))
    Email = Column(String(255), unique=True)
    FirstName = Column(String(255))
    LastName = Column(String(255))
    City = Column(String(255))
    Birthday = Column(String(10))
    SignUpDate = Column(String(10))

    def __init__(self,ID, username, password, email, firstName, lastName, city, birthday, signupdate):
        self.ID = ID
        self.Username = username
        self.UPassword = password
        self.Email = email
        self.FirstName = firstName
        self.LastName = lastName
        self.City = city
        self.Birthday = birthday
        self.SignUpDate = signupdate

    def __repr__(self):
        return '<Player %r>' % (self.Username)

    def setAddedDate(self):
        self.addedDateObject = datetime.strptime(self.SignUpDate, "%d/%m/%Y")

    def calculateAge(self):
        birthdate = datetime.strptime(self.Birthday, "%d/%m/%Y")
        today = date.today()
        self.age = today.year - birthdate.year - ((today.month, today.day) < (birthdate.month, birthdate.day))

class Court(DBase):
    __tablename__ = 'courts'
    ID = Column(Integer, primary_key=True)
    name = Column(String(255), unique=True)
    city = Column(String(255))
    street = Column(String(255))
    streetNum = Column(Integer)
    kind = Column(String(255))
    width = Column(Integer)
    length = Column(Integer)
    description = Column(String(511))
    phone = Column(String(10))
    addedDate = Column(String(10))
    LA = Column(DOUBLE())
    LO = Column(DOUBLE())

    def __init__(self, ID, name, city, street, streetNum, kind, width, length, description, phone, LA, LO, addedDate):
        self.ID = ID
        self.name = name
        self.city = city
        self.street = street
        self.streetNum = streetNum
        self.kind = kind
        self.width = width
        self.length = length
        self.description = description
        self.phone = phone
        self.addedDate = addedDate
        self.LA = LA
        self.LO = LO

    def __repr__(self):
        return '<Court %r>' % (self.name)

    def setAddedDate(self):
        self.addedDateObject = datetime.strptime(self.addedDate, "%d/%m/%Y")

class Game(DBase):
    __tablename__ = 'games'
    ID = Column(Integer, primary_key=True)
    creator = Column(Integer)
    date = Column(String(10))
    time = Column(String(5))
    numberOfPlayers = Column(Integer)
    private = Column(Boolean)
    duration = Column(Integer)
    addedDate = Column(String(10))

    def __init__(self, ID, creator, date, time, numberOfPlayers, private, duration, addedDate):
        self.ID = ID
        self.creator = creator
        self.date = date
        self.time = time
        self.numberOfPlayers = numberOfPlayers
        self.private = private
        self.duration = duration
        self.addedDate = addedDate

    def __repr__(self):
        return '<Game %r>' % (str(self.ID))

    def getCourtID(self):
        courtid = None
        query = GameCourt.query.filter(GameCourt.IDGame == self.ID)
        if (query):
            gamecourt = query.first()
            if (gamecourt):
                courtid = gamecourt.IDCourt
        if (courtid):
            self.courtID = courtid
        else:
            self.courtID = "לא זמין"
        return courtid

    def setCourtIDandName(self):
        courtname = None
        courtid = self.getCourtID()
        if (courtid):
            query = Court.query.filter(Court.ID == courtid)
            if (query):
                court = query.first()
                if (court):
                    courtname = court.name
        if (courtname):
            self.courtName = courtname
        else:
            self.courtName = "לא זמין"

    def setCreatorUsername(self):
        creatorun = None
        query = Player.query.filter(Player.ID == self.creator)
        if (query):
            creator = query.first()
            if (creator):
                creatorun = creator.Username
        if (creatorun):
            self.creatorUsername = creatorun
        else:
            self.creatorUsername = "לא זמין"

    def setAddedDate(self):
        self.addedDateObject = datetime.strptime(self.addedDate, "%d/%m/%Y")

class GameCourt(DBase):
    __tablename__ = 'gamecourt'
    IDGame = Column(Integer, primary_key=True)
    IDCourt = Column(Integer)

    def __init__(self, idgame, idcourt):
        self.IDCourt = idcourt
        self.IDGame = idgame

def getTodaysDate():
    return time.strftime("%d/%m/%Y")

def verifyAdminPassword(username, password):
    init_db()
    query = Admin.query.filter(Admin.Username == username)
    adminString = None
    if (query):
        admin = query.first()
        if (admin):
            if (admin.UPassword == password):
                adminString = admin.Username + ':' + str(admin.ID)
    return adminString

def getAdminUid(username):
    init_db()
    query = Admin.query.filter(Admin.Username == username)
    admin = None
    if (query):
        admin = query.first()
        if (admin):
            admin = admin.ID
    return admin

def getlastNGames(N):
    games = Game.query.all()
    if (games):
        for game in games:
            game.setAddedDate()
        games.sort(key=lambda x: x.addedDateObject, reverse=True)
        games = games[:N]
        for game in games:
            game.setCourtIDandName()
            game.setCreatorUsername()
        return games
    else:
        return None

def getlastNPlayers(N):
    players = Player.query.all()
    if (players):
        for player in players:
            player.setAddedDate()
        players.sort(key=lambda x: x.addedDateObject, reverse=True)
        players = players[:N]
        for player in players:
            player.calculateAge()
        return players
    else:
        return None

def getlastNCourts(N):
    courts = Court.query.all()
    if (courts):
        for court in courts:
            court.setAddedDate()
        courts.sort(key=lambda x: x.addedDateObject, reverse=True)
        courts = courts[:N]
        return courts
    else:
        return None

def getUsersForPage(page, perPage,firstName=None,
                    lastName=None,city=None, ageMin=None,ageMax=None,
                    sortBy=None,uid=None,username=None, reverse=False):

    players = Player.query
    if (firstName):
        firstName = "%" + firstName + "%"
        players = players.filter(Player.FirstName.like(firstName))
    if (lastName):
        lastName = "%" + lastName + "%"
        players = Player.query.filter(Player.LastName.like(lastName))
    if (city):
        players = players.filter(Player.City == city)
    if (uid):
        players = players.filter(Player.ID == uid)
    if (username):
        username = "%" + username + "%"
        players = players.filter(Player.Username.like(username))
    if (players):
        players = players.all()
        for player in players:
            player.calculateAge()
        if (ageMin):
            players = [x for x in players if x.age >= ageMin]
        if (ageMax):
            players = [x for x in players if x.age <= ageMax]
        count = len(players)
        numOfPages = (count // perPage)
        if (count % perPage >= 1):
            numOfPages += 1
        if (numOfPages < page):
            return None, count
        else:
            if (sortBy):
                if (sortBy == "username"):
                    players.sort(key=lambda x: x.Username, reverse=reverse)
                elif (sortBy == "age"):
                    for player in players:
                        player.calculateAge()
                    players.sort(key=lambda x: x.age, reverse=reverse)
                elif (sortBy == "city"):
                    players.sort(key=lambda x: x.City, reverse=reverse)
                elif (sortBy == "firstName"):
                    players.sort(key=lambda x: x.FirstName, reverse=reverse)
                elif (sortBy == "lastName"):
                    players.sort(key=lambda x: x.LastName, reverse=reverse)
                else:
                    for player in players:
                        player.setAddedDate()
                    players.sort(key=lambda x: x.addedDateObject, reverse=reverse)

            page -=1
            skipFirst = perPage*page
            trimLast = skipFirst + perPage
            return players[skipFirst:trimLast], count
    return None, 0

def getPlayerByUid(uid):
    query = Player.query.filter(Player.ID == uid)
    if query:
        player = query.first()
        if player:
            return player
        else:
            return None

def updateCourtByUid(uid, form):
    session = db_session()
    court = session.query(Court).filter(Court.ID == uid).first()
    toUpdate = {"name": form.name.data,
                "street": form.street.data,
                "num": form.num.data,
                "kind":form.kind.data,
                "width": form.width.data,
                "length": form.length.data,
                "description": form.description.data,
                "phone": form.phone.data,
                "LA": form.LA.data,
                "LO": form.LO.data,
                "city": form.city.data}
    toUpdate = {k: v for k, v in toUpdate.items() if v != None and v != ""}
    if ("name" in toUpdate.keys()):
        court.name = toUpdate["name"]
    if ("street" in toUpdate.keys()):
        court.street = toUpdate["street"]
    if ("num" in toUpdate.keys()):
        court.streetNum = toUpdate["num"]
    if ("city" in toUpdate.keys()):
        court.city = toUpdate["city"]
    if ("kind" in toUpdate.keys()):
        court.kind = toUpdate["kind"]
    if ("width" in toUpdate.keys()):
        court.width = toUpdate["width"]
    if ("length" in toUpdate.keys()):
        court.length = toUpdate["length"]
    if ("phone" in toUpdate.keys()):
        court.phone = toUpdate["phone"]
    if ("description" in toUpdate.keys()):
        court.description = toUpdate["description"]
    if ("LA" in toUpdate.keys()):
        court.LA = toUpdate["LA"]
    if ("LO" in toUpdate.keys()):
        court.LO = toUpdate["LO"]
    session.flush()
    session.commit()
    session.close()


def getCourtByUid(uid):
    query = Court.query.filter(Court.ID == uid)
    if query:
        court = query.first()
        if court:
            return court
        else:
            return None

def deleteCourt(uid):
    session = db_session()
    courts = session.query(Court).filter(Court.ID == uid)

    if (courts):
        courtToDelete = courts.first()
        session.delete(courtToDelete)
        session.commit()
        session.flush()
        session.close()
        return True
    else:
        return False

def deleteUser(uid):
    session = db_session()
    players = session.query(Player).filter(Player.ID == uid)

    if (players):
        playerToDelete = players.first()
        session.delete(playerToDelete)
        session.commit()
        session.flush()
        session.close()
        return True
    else:
        return False

def deleteAdmin(uid):
    session = db_session()
    admins = session.query(Admin).filter(Admin.ID == uid)

    if (admins):
        adminToDelete = admins.first()
        session.delete(adminToDelete)
        session.commit()
        session.flush()
        session.close()
        return True
    else:
        return False

def updateUserByUid(uid,form):
    session = db_session()
    player = session.query(Player).filter(Player.ID == uid).first()
    toUpdate = {"username": form.username.data,
                "password": form.password.data,
                "email": form.email.data,
                "firstname":form.firstname.data,
                "lastname": form.lastname.data,
                "city": form.city.data,
                "birthdate": form.birthdate.data}
    toUpdate = {k: v for k, v in toUpdate.items() if v != None and v != ""}
    if ("username" in toUpdate.keys()):
        player.Username = toUpdate["username"]
    if ("password" in toUpdate.keys()):
        player.UPassword = toUpdate["password"]
    if ("email" in toUpdate.keys()):
        player.Email = toUpdate["email"]
    if ("city" in toUpdate.keys()):
        player.City = toUpdate["city"]
    if ("firstname" in toUpdate.keys()):
        player.FirstName = toUpdate["firstname"]
    if ("lastname" in toUpdate.keys()):
        player.LastName = toUpdate["lastname"]
    if ("birthdate" in toUpdate.keys()):
        player.Birthday = toUpdate["birthdate"]
    session.flush()
    session.commit()
    session.close()

def AddNewPlayer(form):
    session = db_session()
    player = Player(0,
                    form.username.data,
                    form.password.data,
                    form.email.data,
                    form.firstname.data,
                    form.lastname.data,
                    form.city.data,
                    form.birthdate.data,
                    getTodaysDate())
    session.add(player)
    session.flush()
    session.commit()
    session.close()

def AddNewCourt(form):
    session = db_session()
    kind = None
    street = None
    description = None
    phone = None
    if (form.kind.data != ""):
        kind = form.kind.data
    if (form.street.data != ""):
        street = form.street.data
    if (form.description.data != ""):
        description = form.description.data
    if (form.phone.data != ""):
        phone = form.description.data
    court = Court(0,
                    form.name.data,
                    form.city.data,
                    street,
                    form.num.data,
                    kind,
                    form.width.data,
                    form.length.data,
                    description,
                    phone,
                    float(form.LA.data),
                    float(form.LO.data),
                    getTodaysDate())
    session.add(court)
    session.flush()
    session.commit()
    session.close()

def AddNewAdmin(form):
    session = db_session()
    admin = Admin(form.username.data,
                    form.password.data,
                    0,
                    form.email.data,
                    form.permissionlevel.data,
                    getTodaysDate())
    session.add(admin)
    session.flush()
    session.commit()
    session.close()

def getAdminPermissionLevel(uid):
    query = Admin.query.filter(Admin.ID == uid)
    if (query):
        admin = query.first()
        if (admin):
            return admin.permissionLevel
        else:
            return None

def getCourtsForPage(page, perPage, sortBy=None,num=None,street=None,
                                     city=None,kind=None,
                                   uid=None,courtname=None, reverse=False):
    courts = Court.query
    if (uid):
        courts = courts.filter(Court.ID == uid)
    if (courtname):
        courtname = "%" + courtname + "%"
        courts = courts.filter(Court.name.like(courtname))
    if (num):
        courts = courts.filter(Court.streetNum == num)
    if (street):
        street = "%" + street + "%"
        courts = courts.filter(Court.street.like(street))
    if (city):
        city = "%" + city + "%"
        courts = courts.filter(Court.city.like(city))
    if (kind):
        courts = courts.filter(Court.kind == kind)
    if (courts):
        courts = courts.all()
        count = len(courts)
        numOfPages = (count // perPage)
        if (count % perPage >= 1):
            numOfPages += 1
        if (numOfPages < page):
            return None, count
        else:
            if (sortBy):
                if (sortBy == "name"):
                    courts.sort(key=lambda x: x.name, reverse=reverse)
                elif (sortBy == "street"):
                    courts.sort(key=lambda x: x.street, reverse=reverse)
                elif (sortBy == "num"):
                    courts.sort(key=lambda x: x.streetNum, reverse=reverse)
                elif (sortBy == "city"):
                    courts.sort(key=lambda x: x.city, reverse=reverse)
                else:
                    for court in courts:
                        court.setAddedDate()
                    courts.sort(key=lambda x: x.addedDateObject, reverse=reverse)
            page -=1
            skipFirst = perPage*page
            trimLast = skipFirst + perPage
            return courts[skipFirst:trimLast], count
    return None, 0


def getAdminsForPage(page, perPage, sortBy=None,permissionlevel=None,
                                   uid=None,username=None, reverse=False):
    admins = Admin.query
    if (uid):
        admins = admins.filter(Admin.ID == uid)
    if (username):
        username = "%" + username + "%"
        admins = admins.filter(Admin.Username.like(username))
    if (permissionlevel):
        admins = admins.filter(Admin.permissionLevel == permissionlevel)
    if (admins):
        admins = admins.all()
        count = len(admins)
        numOfPages = (count // perPage)
        if (count % perPage >= 1):
            numOfPages += 1
        if (numOfPages < page):
            return None, count
        else:
            if (sortBy):
                if (sortBy == "username"):
                    admins.sort(key=lambda x: x.Username, reverse=reverse)
                elif (sortBy == "permissionlevel"):
                    admins.sort(key=lambda x: x.permissionLevel, reverse=reverse)
                else:
                    for admin in admins:
                        admin.setAddedDate()
                    admins.sort(key=lambda x: x.addedDateObject, reverse=reverse)
            page -=1
            skipFirst = perPage*page
            trimLast = skipFirst + perPage
            return admins[skipFirst:trimLast], count
    return None, 0

if __name__ == "__main__":
    from forms import EditUserForm
    form = EditUserForm()
    try:
        updateUserByUid(1,form)
    except Exception as e:
        if 'Email' in e.args[0]:
            print('דואר אלקטרוני כבר קיים במערכת')
        if 'Username' in e.args[0]:
            print ('שם משתמש זה כבר קיים במערכת')