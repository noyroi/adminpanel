from wtforms import Form,DateField ,SelectField, StringField, PasswordField, validators, SubmitField, IntegerField, HiddenField
required = 'שדה חובה'
length = 'בין 4 ל-25 תווים'
date = 'תאריך בפורמט dd/mm/yyyy'
match = 'הסיסמאות לא תואמות'
permission = 'ערכים אפשריים: 1 - ללא עריכת מנהלים, 2 - הוספת מנהלים בלבד, 3 - הוספת ומחיקת מנהלים'
class LoginForm(Form):
    username = StringField('שם משתמש', [validators.Length(message=length,min=4, max=25),validators.data_required(message=required)])
    password = PasswordField('סיסמא', [
        validators.DataRequired(message=required), validators.Length(message=length,min=4, max=25)
    ])
    submit = SubmitField('שלח')

class SearchByUsername(Form):
    username = StringField('שם משתמש', [validators.Length(message=length,min=4, max=25),validators.data_required(message=required)])
    submit = SubmitField("חפש")

class SearchByCourtName(Form):
    courtname = StringField('שם מגרש', [validators.Length(message=length,min=4, max=25),validators.data_required(message=required)])
    submit = SubmitField("חפש")

class SearchUserByUid(Form):
    uid = StringField('מזהה משתמש', [validators.Length(message=length,min=4, max=25),validators.data_required(message=required)])
    submit = SubmitField("חפש")

class SearchCourtByUid(Form):
    uid = StringField('מזהה מגרש', [validators.Length(message=length,min=4, max=25),validators.data_required(message=required)])
    submit = SubmitField("חפש")

class CustomUserSearch(Form):
    firstname = StringField('שם פרטי')
    lastname = StringField('שם משפחה')
    city = StringField('עיר מגורים')
    agemin = IntegerField('מגיל')
    agemax = IntegerField('עד גיל')
    submit = SubmitField("חפש")

class CustomCourtSearch(Form):
    street = StringField('רחוב')
    num = IntegerField('מספר')
    city = StringField('עיר')
    kind = SelectField('סוג מגרש', choices =[("", 'סוג'),('A', 'אספלט'), ('G', 'דשא'), ('S', 'חול')], default="")
    submit = SubmitField("חפש")

class EditUserForm(Form):
    username = StringField('שם משתמש', [validators.Length(message=length,min=4, max=25),validators.data_required(message=required)])
    firstname = StringField('שם פרטי', [validators.data_required(message=required)])
    lastname = StringField('שם משפחה', [validators.data_required(message=required)])
    city = StringField('עיר מגורים', [validators.data_required(message=required)])
    birthdate = StringField('תאריך לידה',[validators.data_required(message=required)])
    email = StringField('דואר אלקטרוני', [validators.Length(message=length,min=4, max=25),validators.data_required(message=required)])
    password = PasswordField('סיסמא')
    submit = SubmitField("עדכן")

class AddUserForm(Form):
     username = StringField('שם משתמש', [validators.Length(message=length, min=4, max=25),
                                         validators.data_required(message=required)])
     firstname = StringField('שם פרטי', [validators.data_required(message=required)])
     lastname = StringField('שם משפחה', [validators.data_required(message=required)])
     city = StringField('עיר מגורים', [validators.data_required(message=required)])
     birthdate = StringField('תאריך לידה', [validators.data_required(message=required)])
     email = StringField('דואר אלקטרוני', [validators.Length(message=length, min=4, max=25),
                                           validators.data_required(message=required)])
     validatepassword = PasswordField('חזור על הסיסמא', [validators.data_required(message=required)])
     password = PasswordField('סיסמא',[validators.data_required(message=required), validators.EqualTo('validatepassword',message=match)])

     submit = SubmitField("שלח")

class AddAdminForm(Form):
    username = StringField('שם משתמש', [validators.Length(message=length, min=4, max=25),
                                        validators.data_required(message=required)])
    email = StringField('דואר אלקטרוני', [validators.Length(message=length, min=4, max=25),
                                          validators.data_required(message=required)])
    validatepassword = PasswordField('חזור על הסיסמא', [validators.data_required(message=required)])
    password = PasswordField('סיסמא', [validators.data_required(message=required),
                                       validators.EqualTo('validatepassword', message=match)])
    permissionlevel = IntegerField('רמת הרשאה',[validators.number_range(min=1, max=3, message=permission)])
    submit = SubmitField("שלח")

class AddCourtForm(Form):
    name = StringField('שם מגרש', [validators.Length(message=length, min=4, max=25),
                                        validators.data_required(message=required)])
    city = StringField('עיר', [validators.data_required(message=required)])
    street = StringField('רחוב')
    num = IntegerField('מספר',[validators.number_range(min=1, max=1000, message='ערך מספרי מ-1 עד 999'), validators.optional()])
    kind = SelectField('סוג מגרש', choices =[("", 'סוג'),('A', 'אספלט'), ('G', 'דשא'), ('S', 'חול')], default='')
    width = IntegerField('רוחב', [validators.number_range(min=10, max=1000, message='ערך מספרי מ-10 עד 1000'), validators.optional()])
    length = IntegerField('אורך', [validators.number_range(min=10, max=1000, message='ערך מספרי מ-10 עד 1000'), validators.optional()])
    description = StringField('תיאור')
    phone = StringField('מספר טלפון')
    LA = HiddenField()
    LO = HiddenField()
    submit = SubmitField("שלח")
    verify = SubmitField("אמת כתובת")